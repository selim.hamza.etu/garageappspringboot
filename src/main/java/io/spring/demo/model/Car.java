package io.spring.demo.model;

public class Car {

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public enum Color{
        RED, BLACK, WHITE
    }
    private int id;
    private String model;
    private String marque;
    private int year;
    private Color color;
    public Car() {}

    public Car(int id, String model, String marque, int year, Color color) {
        super();
        this.id = id;
        this.model = model;
        this.marque = marque;
        this.year = year;
        this.color = color;
    }

}
