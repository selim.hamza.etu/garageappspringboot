package io.spring.demo;

import io.spring.demo.model.Car;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class GarageAppApplication {


    public static void main(String[] args) {
        SpringApplication.run(GarageAppApplication.class, args);
    }

}
