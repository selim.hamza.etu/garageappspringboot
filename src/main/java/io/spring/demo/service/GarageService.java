package io.spring.demo.service;

import io.spring.demo.model.Car;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class GarageService {

    private static List<Car> cars = new ArrayList<>(Arrays.asList(
            new Car(1,"Aventador","Lamborghini",2020, Car.Color.BLACK),
            new Car(2,"Porche","Cayenne",2020, Car.Color.WHITE)
    ));

    public static List<Car> getCars() {
        return cars;
    }

    public Car getCar(int id) {
        //Filtre mon tableau en verifiant si getId de ma voiture == id
        return cars.stream().filter(car -> car.getId() == id).findFirst().orElse(null);
    }

    public void add(Car car) {
        if (!exists(car.getId())) {
            cars.add(car);
        }
    }

    public static boolean exists(int id) {
        return cars.stream().anyMatch(car -> car.getId() == id);
    }

    public void remove(int id)
    {
        cars.removeIf(car -> car.getId() == id);
    }

    public void update(Car newCar, int id)
    {
        for (Car car1 : cars) {
            if (car1.getId() == id) {
                cars.set(cars.indexOf(car1), newCar);
            }
        }
    }
}
