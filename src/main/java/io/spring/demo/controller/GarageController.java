package io.spring.demo.controller;

import io.spring.demo.model.Car;
import io.spring.demo.service.GarageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GarageController {

    @Autowired //Injecter variales de notre garageService automatiquement
    GarageService garageService;

    @RequestMapping(method = RequestMethod.GET,value = "cars")
    public List<Car> getCars()
    {
       return garageService.getCars();
    }

    @RequestMapping(method = RequestMethod.GET,value = "car/{id}")
    public Car getCarById(@PathVariable int id)
    {
        return garageService.getCar(id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "car/{id}")
    public void deletCar(@PathVariable int id)
    {
        garageService.remove(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "cars")
    public void addCar(@RequestBody Car car) //Doit avoir body de Car en JSON
    {
        garageService.add(car);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "car/{id}")
    public void updateCar(@RequestBody Car newCar, @PathVariable int id)
    {
        garageService.update(newCar,id);
    }


}
